import {Injectable} from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs';

import {IBook} from '../../models/book.model';

@Injectable()
export class CaretService {
  books = [];

  private subject = new BehaviorSubject<any>(this.books);

  constructor() {}

  addBook(data: IBook) {
    this.books.push(data);
    this.subject.next(this.books);
  }

  deleteBook(book) {

    

    this.books = this.books.filter((item) => {
      return item.isbn != book.isbn;
    });

    console.log('filtered: ', this.books);
    
    this.subject.next(this.books);
  }

  getBooks(): Observable<IBook[]> {
    return this.subject.asObservable();
  }
}