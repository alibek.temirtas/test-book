import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {IBook} from '../../models/book.model';

import * as booksJson from '../../data/books.data.json';

// MODELS :

@Injectable()
export class BookService {
    private _booksJson: IBook[] = booksJson.default;
    private $bookSubject: BehaviorSubject<any | null> = new BehaviorSubject(this._booksJson);
    
    constructor() {
    }

    getBooks(): Observable<IBook[]> {
        return this.$bookSubject.asObservable();
    }

    searchBook(options): void {

        let filteredBooks = this._booksJson.filter((item) => {
            return options.searchTxt ? item.title.toLowerCase().indexOf(options.searchTxt.toLowerCase()) > -1  : true;
        });

        this.$bookSubject.next(filteredBooks);
    }
}
