import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'cutPrice'
})
export class ConvertPricePipe implements PipeTransform {
  transform(value: any, args?: any): number {
    value = value + '';
    return value.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ').replace(/\.0+/, '');
  }
}