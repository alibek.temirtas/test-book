import {Component, Input, Output, EventEmitter} from '@angular/core';

import {IBook} from '../../../models/book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './view.html'
})
export class BooksListComponent{
  @Input() books: IBook[];
  @Input() isCaret?: boolean = false;
  @Output() emitAddBook: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  addBook(item: IBook){
    this.emitAddBook.emit(item);
  }
}