import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

// SHARED COMPONENTS :
import {BooksListComponent} from './components/books-list/component';

// PIPES :
import {ConvertPricePipe} from './pipes/convert-price.pipes';

// SERVICES :
import {BookService} from './_services/book.service';
import {CaretService} from './_services/caret.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    ConvertPricePipe,
    BooksListComponent
  ],
  providers: [
    BookService,
    CaretService
  ],
  exports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ConvertPricePipe,
    BooksListComponent
  ]
})

export class SharedModule {
}
