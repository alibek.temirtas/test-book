import { Component, OnInit } from '@angular/core';

import { BookService } from './_shared/_services/book.service';
import { CaretService } from './_shared/_services/caret.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit{
  searchTxt: string = '';
  title = 'test-book';

  totalCount: number = 0;

  constructor(private _bookService: BookService,
              private _caretSerivce: CaretService) {

  }

  ngOnInit(){
    this._caretSerivce.getBooks().subscribe((data)=>{
      this.totalCount = data.length;
    });
  }

  searchBook(): void {
    this._bookService.searchBook({ searchTxt: this.searchTxt });
  }
}
