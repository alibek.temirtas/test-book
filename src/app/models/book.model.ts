export interface IBook {
    id: number;
    title: string;
    description: string;
    price: number;
    isbn: string;
    pageCount: number;
    publishedDate: {
        date: string
    }
    thumbnailUrl: string;
    shortDescription: string;
    longDescription: string;
    status: string;
    authors: Array<string>;
    categories: Array<string>;
}

export class Book {
    id: number;
    title: string;
    description: string;
    price: number;
    isbn: string;
    pageCount: number;
    publishedDate: {
        date: string
    };
    thumbnailUrl: string;
    shortDescription: string;
    longDescription: string;
    status: string;
    authors: Array<string>;
    categories: Array<string>;

    constructor(options: {
        id: number,
        title: string,
        description: string,
        price: number,
        isbn: string,
        pageCount: number,
        publishedDate: {
            date: string
        },
        thumbnailUrl: string,
        shortDescription: string,
        longDescription: string,
        status: string,
        authors: Array<string>,
        categories: Array<string>
    }) {
        for (let key in options) {
            this[key] = options[key];
        }
    }
}