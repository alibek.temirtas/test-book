import {Component, OnInit, OnDestroy} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import {BookService} from '../../_shared/_services/book.service';
import {CaretService} from '../../_shared/_services/caret.service';

import {IBook} from '../../models/book.model';

@Component({
  selector: 'app-root-books',
  templateUrl: './view.html'
})
export class RootBooksComponent implements OnInit, OnDestroy{
  books: IBook[] = [];

  private _onDestroy = new Subject();

  constructor(private _bookService: BookService, 
              private _caretService: CaretService){
  }

  ngOnInit(): void {
    this.getBooks();
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

  getBooks(): void{
    this._bookService.getBooks().pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this.books = data;
    });
  }

  addBookToCaret(book: IBook): void{
    this._caretService.addBook(book);
  }

}
