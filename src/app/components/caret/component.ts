import {Component, OnInit, OnDestroy} from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import {CaretService} from '../../_shared/_services/caret.service';

import {IBook, Book} from '../../models/book.model';

@Component({
  selector: 'app-root-caret',
  templateUrl: './view.html'
})
export class RootCaretComponent implements OnInit, OnDestroy{
  books: IBook[] = [];
  totalPrice: number = 0;

  private _onDestroy = new Subject();

  constructor(private _caretService: CaretService){
  }

  ngOnInit(): void {
    this.getBooks();
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
  }

  getBooks(): void{
    this._caretService.getBooks().pipe(takeUntil(this._onDestroy)).subscribe((data)=>{
      this.books = data;
      this.totalPrice = 0;
      this.books.forEach((book)=>{
        this.totalPrice += +book.price;
      });
    });
  }

  deleteBookFromCaret(book: IBook): void{
    this.totalPrice = this.totalPrice - book.price;

    this._caretService.deleteBook(book);
  }

}
