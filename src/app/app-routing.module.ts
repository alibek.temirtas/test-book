import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './_shared/shared.module';

// COMPONENTS :
import { RootBooksComponent } from './components/books/component';
import { RootCaretComponent } from './components/caret/component';

const routes: Routes = [
  { path: 'books', component: RootBooksComponent },
  { path: 'caret', component: RootCaretComponent },
  { path: '', redirectTo: 'books', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    RootBooksComponent,
    RootCaretComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
    SharedModule
  ]
})
export class AppRoutingModule { }
